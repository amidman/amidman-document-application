package com.example.amiddocapp.di

import com.example.amiddocapp.di.repository.loginRepositoryModule
import com.example.amiddocapp.di.repository.studentRepositoryModule
import com.example.amiddocapp.di.repository.studentRequestRepositoryModule
import org.koin.core.KoinApplication

fun KoinApplication.repositoryModules(){
    modules(
        loginRepositoryModule,
        studentRequestRepositoryModule,
        studentRepositoryModule
    )
}