package com.example.amiddocapp.di.repository

import com.example.amiddocapp.data.api.student.StudentRepositoryImpl
import com.example.amiddocapp.domain.repository.StudentRepository
import org.koin.dsl.module

val studentRepositoryModule = module {
    single<StudentRepository>{
        StudentRepositoryImpl(
            client = get(),
            tokenRepository = get()
        )
    }
}