package com.example.amiddocapp.di

import android.app.Application
import com.example.amiddocapp.di.basic.apiModule
import com.example.amiddocapp.di.basic.navigationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class DIApplication:Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext( this@DIApplication)
            repositoryModules()
            basicModules()
            viewModelModules()
        }
    }
}