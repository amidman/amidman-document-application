package com.example.amiddocapp.di.viewmodel

import com.example.amiddocapp.ui.screens.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module{
    viewModel<LoginViewModel>{
        LoginViewModel(
            loginRepository = get(),
        )
    }
}