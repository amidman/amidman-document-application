package com.example.amiddocapp.di

import com.example.amiddocapp.di.viewmodel.loginModule
import com.example.amiddocapp.di.viewmodel.studentModule
import org.koin.core.KoinApplication

fun KoinApplication.viewModelModules(){
    modules(
        loginModule,
        studentModule
    )
}