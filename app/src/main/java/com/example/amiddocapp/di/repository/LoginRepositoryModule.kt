package com.example.amiddocapp.di.repository

import com.example.amiddocapp.data.api.login.LoginRepositoryImpl
import com.example.amiddocapp.data.datastore.auth.TokenRepositoryImpl
import com.example.amiddocapp.domain.repository.LoginRepository
import com.example.amiddocapp.domain.repository.TokenRepository
import org.koin.dsl.module

val loginRepositoryModule = module {

    single<LoginRepository>{
        LoginRepositoryImpl(
            tokenRepository = get(),
            client = get()
        )
    }

    single<TokenRepository>{
        TokenRepositoryImpl(
            context = get()
        )
    }
}