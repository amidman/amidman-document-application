package com.example.amiddocapp.di.basic

import com.example.amiddocapp.data.api.BaseRoutes.BASE_URL
import com.example.amiddocapp.ui.navigation.MainNavigation
import com.example.amiddocapp.ui.screens.destinations.LoginScreenDestination
import com.ramcosta.composedestinations.spec.DirectionDestinationSpec
import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import org.koin.dsl.module

val apiModule = module {
    single<HttpClient> {
        val mainNavigation = get<MainNavigation>()
        HttpClient(OkHttp){
            install(ContentNegotiation){
                json()
            }
            HttpResponseValidator {
                validateResponse {
                    if (it.status == HttpStatusCode.Unauthorized)
                        mainNavigation.navToDestination(LoginScreenDestination.invoke() as DirectionDestinationSpec)
                }

            }

            defaultRequest {
                contentType(ContentType.Application.Json)
                url(BASE_URL)
            }
        }
    }
}