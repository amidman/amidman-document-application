package com.example.amiddocapp.di.basic

import com.example.amiddocapp.ui.navigation.MainNavigation
import org.koin.dsl.module

val navigationModule = module {
    single<MainNavigation> {
        MainNavigation()
    }
}