package com.example.amiddocapp.di.repository

import com.example.amiddocapp.data.api.student_request.StudentRequestRepositoryImpl
import com.example.amiddocapp.domain.repository.StudentRequestRepository
import org.koin.dsl.module

val studentRequestRepositoryModule = module {
    single<StudentRequestRepository>{
        StudentRequestRepositoryImpl(
            client = get(),
            tokenRepository = get()
        )
    }
}