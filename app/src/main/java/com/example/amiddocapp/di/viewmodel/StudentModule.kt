package com.example.amiddocapp.di.viewmodel

import com.example.amiddocapp.ui.screens.student_request.RequestViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val studentModule = module {
    viewModel<RequestViewModel>{
        RequestViewModel(
            requestRepository = get()
        )
    }
}