package com.example.amiddocapp.di

import com.example.amiddocapp.di.basic.apiModule
import com.example.amiddocapp.di.basic.navigationModule
import org.koin.core.KoinApplication

fun KoinApplication.basicModules(){
    modules(
        apiModule,
        navigationModule
    )
}