package com.example.amiddocapp.data.api.student

import com.example.amiddocapp.data.api.catchResponseException
import com.example.amiddocapp.data.datastore.auth.setHeader
import com.example.amiddocapp.domain.model.network_base.RequestStatus
import com.example.amiddocapp.domain.repository.StudentRepository
import com.example.amiddocapp.domain.repository.TokenRepository
import com.example.domain.model.student.StudentModel
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class StudentRepositoryImpl(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository
):StudentRepository {
    override suspend fun studentModel(): RequestStatus<StudentModel> = catchResponseException {
        client.get(StudentRoutes.INFO){
            setHeader(tokenRepository.getToken())
        }.toRequestStatus()
    }


}