package com.example.amiddocapp.data.api.login

import com.example.amiddocapp.data.api.BaseRoutes

object LoginRoutes {

    private const val base = BaseRoutes.USER_ROUTE

    const val LOGIN_ROUTE = base + "login"

}