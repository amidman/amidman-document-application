package com.example.amiddocapp.data.api.student_request

import com.example.amiddocapp.data.api.catchResponseException
import com.example.amiddocapp.data.datastore.auth.setHeader
import com.example.amiddocapp.domain.model.network_base.RequestStatus
import com.example.amiddocapp.domain.model.student_request.StudentRequestDTO
import com.example.amiddocapp.domain.repository.StudentRequestRepository
import com.example.amiddocapp.domain.repository.TokenRepository
import com.example.amiddocapp.domain.model.student_request.RequestCreateModel
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class StudentRequestRepositoryImpl(
    private val client: HttpClient,
    private val tokenRepository: TokenRepository,
):StudentRequestRepository {
    override suspend fun studentRequests(): RequestStatus<List<StudentRequestDTO>> = catchResponseException {
        client.get(RequestRoutes.GET_BY_STUDENT){
            setHeader(tokenRepository.getToken())
        }.toRequestStatus()
    }

    override suspend fun createStudentRequest(requestCreateModel: RequestCreateModel): RequestStatus<StudentRequestDTO> =
        catchResponseException{
        client.post(RequestRoutes.CREATE) {
            setHeader(tokenRepository.getToken())
            setBody(requestCreateModel)
        }.toRequestStatus()
    }

    override suspend fun declineStudentRequest(requestId: Int):RequestStatus<Unit> = catchResponseException {
        client.delete(RequestRoutes.DECLINE){
            setHeader(tokenRepository.getToken())
            url{
                parameters.append(RequestRoutes.studentRequestIdQ,requestId.toString())
            }
        }.toRequestStatus()
    }
}