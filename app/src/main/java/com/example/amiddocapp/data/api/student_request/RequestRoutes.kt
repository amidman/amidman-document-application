package com.example.amiddocapp.data.api.student_request

import com.example.amiddocapp.data.api.BaseRoutes

object RequestRoutes {
    const val departmentIdQ = "departmentId"
    const val studentRequestIdQ = "studentRequestId"
    const val studentIdQ = "studentId"
    const val currentDateQ = "currentDate"

    private const val base = BaseRoutes.STUDENT_REQUEST_ROUTE

    const val GET_BY_STUDENT = base + "get-by-student"
    const val CREATE = base + "create"
    const val DECLINE = base + "decline"

}