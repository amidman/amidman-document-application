package com.example.amiddocapp.data.datastore

object DataStoreKeys {
    const val USER_DATASTORE = "UserData"

    const val REFRESH_TOKEN = "refreshToken"

    const val ROLE = "role"

    const val TOKEN = ""

}