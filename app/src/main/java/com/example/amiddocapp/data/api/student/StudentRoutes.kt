package com.example.amiddocapp.data.api.student

import com.example.amiddocapp.data.api.BaseRoutes

object StudentRoutes {


    const val base = BaseRoutes.STUDENT_ROUTE

    const val INFO = base + "info"


}