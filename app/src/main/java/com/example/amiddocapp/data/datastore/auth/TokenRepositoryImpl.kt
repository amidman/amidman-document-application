package com.example.amiddocapp.data.datastore.auth

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.amiddocapp.data.datastore.DataStoreKeys
import com.example.amiddocapp.domain.repository.TokenRepository
import io.ktor.client.request.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class TokenRepositoryImpl(
    private val context: Context
):TokenRepository {

    private val tokenPreference = stringPreferencesKey(DataStoreKeys.TOKEN)

    override suspend fun postToken(token: String) {
        context.userDataStore.edit {
            it[tokenPreference] = token
        }
    }

    override suspend fun getToken(): String {
        val token = context.userDataStore.data.map {
            it[tokenPreference]
        }
        return token.first() ?: ""
    }

}

fun HttpRequestBuilder.setHeader(token: String){
    header("Authorization","Bearer $token")
}