package com.example.amiddocapp.data.datastore.auth

import android.content.Context
import androidx.datastore.preferences.preferencesDataStore
import com.example.amiddocapp.data.datastore.DataStoreKeys.USER_DATASTORE

val Context.userDataStore by preferencesDataStore(name = USER_DATASTORE)