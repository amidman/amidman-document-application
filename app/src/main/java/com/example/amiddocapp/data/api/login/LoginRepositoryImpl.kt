package com.example.amiddocapp.data.api.login

import com.example.amiddocapp.data.api.catchResponseException
import com.example.amiddocapp.data.datastore.auth.setHeader
import com.example.amiddocapp.domain.model.network_base.RequestStatus
import com.example.amiddocapp.domain.repository.LoginRepository
import com.example.amiddocapp.domain.repository.TokenRepository
import com.example.pgk_forum_app.domain.model.login.LoginData
import com.example.pgk_forum_app.domain.model.login.TokenBody
import com.example.pgk_forum_app.domain.utils.toRequestStatus
import io.ktor.client.*
import io.ktor.client.request.*

class LoginRepositoryImpl(
    private val tokenRepository: TokenRepository,
    private val client: HttpClient,
) : LoginRepository {
    override suspend fun login(loginData: LoginData): RequestStatus<TokenBody> =
        catchResponseException {
            val request = client.post(LoginRoutes.LOGIN_ROUTE) {
                setHeader(tokenRepository.getToken())
                setBody(loginData)
            }.toRequestStatus<TokenBody>()
            if (request is RequestStatus.Success)
                tokenRepository.postToken(request.data?.token ?: "")
            request
        }

    override suspend fun quit() {
        tokenRepository.postToken("")
    }
}