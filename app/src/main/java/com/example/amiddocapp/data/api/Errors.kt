package com.example.amiddocapp.data.api

import android.util.Log
import com.example.amiddocapp.domain.model.network_base.ErrorModel
import com.example.amiddocapp.domain.model.network_base.RequestStatus
import io.ktor.client.plugins.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.ConnectException

class Errors<T> {

    val CONNECTION_ERROR =
        RequestStatus.Error<T>(ErrorModel("connection error", "Ошибка подключения"))

    val SERVER_ERROR = RequestStatus.Error<T>(ErrorModel("server error", "Ошибка сервера"))
    val NONAME_ERROR = RequestStatus.Error<T>(ErrorModel("noname error", "Неизвестная Ошибка"))
}

suspend fun <T> catchResponseException(action: suspend CoroutineScope.() -> RequestStatus<T>): RequestStatus<T> =
    withContext(Dispatchers.IO) {
        try {
            action()
        } catch (e: ServerResponseException) {
            Log.e("SERVER ERROR",e.toString())
            Errors<T>().SERVER_ERROR
        } catch (e: ConnectException) {
            Log.e("SERVER ERROR",e.toString())
            Errors<T>().CONNECTION_ERROR
        } catch (e: Exception) {
            Log.e("SERVER ERROR",e.toString())
            Errors<T>().NONAME_ERROR
        }
    }