package com.example.amiddocapp.data.api

object BaseRoutes {

    const val BASE_URL = "http://10.0.2.2:5556"


    const val USER_ROUTE = "/users/"
    const val STUDENT_ROUTE = "/students/"
    const val STUDENT_REQUEST_ROUTE = "/studentRequest/"
}