package com.example.amiddocapp.domain.model.department

import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class DepartmentDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val departmentName:String,
    @Serializable(with = TrimStringSerializer::class)
    val departmentShortName:String,
)