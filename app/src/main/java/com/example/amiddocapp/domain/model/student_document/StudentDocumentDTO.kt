package com.example.amiddocapp.domain.model.student_document

import com.example.serialization.TrimStringSerializer
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class StudentDocumentDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val docNumber:String,
    @Serializable(with = TrimStringSerializer::class)
    val orderNumber:String,
    val orderDate:LocalDate,
    val studyStartDate: LocalDate,
)
