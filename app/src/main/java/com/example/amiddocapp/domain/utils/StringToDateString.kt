package com.example.pgk_forum_app.domain.utils

import kotlinx.datetime.LocalDate
import kotlinx.datetime.toJavaLocalDateTime
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


fun kotlinx.datetime.LocalDateTime.toDateString():String = try {
    val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
    this.toJavaLocalDateTime().format(formatter)
}catch (e:Exception){
    "Ошибка при конвертации"
}

private fun Int.toDoubleDigitFormat():String{
    return if (toString().length < 2)
        "0$this"
    else
        toString()
}

fun LocalDate.toHumanFormat():String {
    return "${dayOfMonth.toDoubleDigitFormat()}.${monthNumber.toDoubleDigitFormat()}.$year"
}