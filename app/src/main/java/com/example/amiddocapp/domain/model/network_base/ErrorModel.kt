package com.example.amiddocapp.domain.model.network_base

import kotlinx.serialization.Serializable


@Serializable
data class ErrorModel(
    val code:String,
    val description:String
)
