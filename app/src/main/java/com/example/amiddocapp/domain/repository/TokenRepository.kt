package com.example.amiddocapp.domain.repository

interface TokenRepository {


    suspend fun getToken():String

    suspend fun postToken(token: String)
}