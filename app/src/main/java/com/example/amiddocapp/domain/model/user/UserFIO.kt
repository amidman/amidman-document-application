package com.example.amiddocapp.domain.model.user

import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class UserFIO(
    @Serializable(with = TrimStringSerializer::class)
    val name:String,
    @Serializable(with = TrimStringSerializer::class)
    val surname:String,
    @Serializable(with = TrimStringSerializer::class)
    val fatherName:String
)
