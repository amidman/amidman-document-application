package com.example.amiddocapp.domain.repository

import com.example.amiddocapp.domain.model.network_base.RequestStatus
import com.example.pgk_forum_app.domain.model.login.LoginData
import com.example.pgk_forum_app.domain.model.login.TokenBody

interface LoginRepository {

    suspend fun login(loginData: LoginData): RequestStatus<TokenBody>

    suspend fun quit()
}