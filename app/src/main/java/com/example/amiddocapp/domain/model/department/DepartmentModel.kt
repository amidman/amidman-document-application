package com.example.domain.model.department

import com.example.amiddocapp.domain.model.department.DepartmentDTO
import com.example.amiddocapp.domain.model.group.GroupDTO
import kotlinx.serialization.Serializable


@Serializable
data class DepartmentModel(
    val department: DepartmentDTO,
    val groupList:List<GroupDTO>
)
