package com.example.amiddocapp.domain.utils

enum class ROLES  {
    GUEST,STUDENT,TEACHER,ADMIN
}