package com.example.amiddocapp.domain.repository

import com.example.amiddocapp.domain.model.network_base.RequestStatus
import com.example.domain.model.student.StudentModel

interface StudentRepository {

    suspend fun studentModel():RequestStatus<StudentModel>


}