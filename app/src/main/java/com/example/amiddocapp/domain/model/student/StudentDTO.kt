package com.example.domain.model.student

import kotlinx.serialization.Serializable


@Serializable
data class StudentDTO(
    val id:Int? = null,
    val userId:Int,
    val groupId:Int,
    val studentDocumentId:Int,
    val addedInfo:String,
)