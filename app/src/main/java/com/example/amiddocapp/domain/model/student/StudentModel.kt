package com.example.domain.model.student

import com.example.amiddocapp.domain.model.department.DepartmentDTO
import com.example.amiddocapp.domain.model.group.GroupDTO
import com.example.amiddocapp.domain.model.student_document.StudentDocumentDTO
import com.example.amiddocapp.domain.model.user.UserDTO
import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable


@Serializable
data class StudentModel(
    val id:Int,
    val user: UserDTO,
    val document: StudentDocumentDTO,
    val group: GroupDTO,
    val department: DepartmentDTO,
    @Serializable(with = TrimStringSerializer::class)
    val addedInfo:String,
)

