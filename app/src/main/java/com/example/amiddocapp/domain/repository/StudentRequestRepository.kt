package com.example.amiddocapp.domain.repository

import com.example.amiddocapp.domain.model.network_base.RequestStatus
import com.example.amiddocapp.domain.model.student_request.StudentRequestDTO
import com.example.amiddocapp.domain.model.student_request.RequestCreateModel

interface StudentRequestRepository {

    suspend fun studentRequests():RequestStatus<List<StudentRequestDTO>>

    suspend fun createStudentRequest(requestCreateModel: RequestCreateModel):RequestStatus<StudentRequestDTO>

    suspend fun declineStudentRequest(requestId:Int): RequestStatus<Unit>
}