package com.example.amiddocapp.domain.model.group

import com.example.amiddocapp.domain.model.group.GroupDTO
import com.example.amiddocapp.domain.model.department.DepartmentDTO
import com.example.domain.model.student.StudentModel
import kotlinx.serialization.Serializable


@Serializable
data class GroupModel(
    val group: GroupDTO,
    val studentList:List<StudentModel>,
    val department: DepartmentDTO
)
