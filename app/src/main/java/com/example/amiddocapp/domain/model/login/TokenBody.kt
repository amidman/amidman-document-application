package com.example.pgk_forum_app.domain.model.login

import kotlinx.serialization.Serializable


@Serializable
data class TokenBody(
    val token:String,
    val role:String,
)
