package com.example.amiddocapp.domain.model.student_request

import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class StudentRequestDTO(
    val id:Int? = null,
    val studentId:Int,
    val status: StudentRequestStatus,
    val type: StudentRequestType,
    val count:Int,
    val date:LocalDate,
    val departmentId:Int,
)

enum class StudentRequestStatus(val value: String){
    SEND("ОТПРАВЛЕНО"),
    ACCEPTED("ПОДТВЕРЖДЕНО"),
}

enum class StudentRequestType(val value:String){
    STUDYDOCUMENT("Справка о месте обучения"),
}
private val typeMap = hashMapOf(
    StudentRequestType.STUDYDOCUMENT.value to StudentRequestType.STUDYDOCUMENT
)

fun String.toRequestType():StudentRequestType?=
    try {
        typeMap[this]
    }catch (e:Exception){
        null
    }


