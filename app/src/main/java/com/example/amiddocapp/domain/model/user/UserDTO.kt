package com.example.amiddocapp.domain.model.user


import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val login:String,
    @Serializable(with = TrimStringSerializer::class)
    val password:String,
    @Serializable(with = TrimStringSerializer::class)
    val name:String,
    @Serializable(with = TrimStringSerializer::class)
    val surname:String,
    @Serializable(with = TrimStringSerializer::class)
    val fatherName:String,
    val role: ROLES
)

enum class ROLES{
    STUDENT,ADMIN,SECRETARY
}



