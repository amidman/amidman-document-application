package com.example.amiddocapp.domain.model.group

import com.example.serialization.TrimStringSerializer
import kotlinx.serialization.Serializable

@Serializable
data class GroupDTO(
    val id:Int? = null,
    @Serializable(with = TrimStringSerializer::class)
    val groupName:String,
    val status: GroupStatus,
    val yearCount:Int,
    val maxYearCount:Int,
    val departmentId:Int
)

enum class GroupStatus{
    BUDGET,
    NOBUGET
}