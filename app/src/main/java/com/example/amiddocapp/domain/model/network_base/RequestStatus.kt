package com.example.amiddocapp.domain.model.network_base

sealed class RequestStatus<T>(val data:T? = null,val error: ErrorModel? = null){
    class Success<T>(data: T): RequestStatus<T>(data)
    class Error<T>(error: ErrorModel): RequestStatus<T>(error = error)
}

