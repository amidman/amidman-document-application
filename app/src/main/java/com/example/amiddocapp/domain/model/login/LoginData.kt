package com.example.pgk_forum_app.domain.model.login

import kotlinx.serialization.Serializable


@Serializable
data class LoginData(
    val login:String,
    val password:String
)
