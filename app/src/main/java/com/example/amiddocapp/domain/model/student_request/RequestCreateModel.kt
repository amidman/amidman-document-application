package com.example.amiddocapp.domain.model.student_request

import com.example.amiddocapp.domain.model.student_request.StudentRequestType
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable


@Serializable
data class RequestCreateModel(
    val count: Int,
    val date: LocalDate,
    val type: StudentRequestType
)