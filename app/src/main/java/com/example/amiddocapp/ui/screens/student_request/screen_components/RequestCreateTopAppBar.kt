package com.example.amiddocapp.ui.screens.student_request.screen_components

import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.ui.common.AmidTitle
import com.example.amiddocapp.ui.common.BackButton
import com.example.amiddocapp.ui.screens.student_request.RequestViewModel
import com.ramcosta.composedestinations.result.ResultBackNavigator

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateRequestTopAppBar(
    backNavigator: ResultBackNavigator<Boolean>,
    requestViewModel: RequestViewModel
) {
    TopAppBar(
        title = {
            AmidTitle(titleString = "Заказ документа")
        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer
        ),
        navigationIcon = {
            BackButton {
                backNavigator.navigateBack()
            }
        },
        actions = {
            TextButton(onClick = requestViewModel::createStudentRequest) {
                AmidTitle(titleString = "Заказать", fontSize = 20.sp)
            }
        }
    )
}