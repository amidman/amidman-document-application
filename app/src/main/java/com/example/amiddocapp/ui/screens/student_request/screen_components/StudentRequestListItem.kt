package com.example.amiddocapp.ui.screens.student_request.screen_components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.domain.model.student_request.StudentRequestDTO
import com.example.amiddocapp.ui.common.AmidTitle
import com.example.amiddocapp.ui.common.Param
import com.example.pgk_forum_app.domain.utils.toHumanFormat


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StudentRequestListItem(
    requestDTO: StudentRequestDTO,
    onDelete: () -> Unit,
) {
    var expanded by remember {
        mutableStateOf(false)
    }
    Card(
        modifier = Modifier
            .padding(10.dp)
            .fillMaxSize()
            .defaultMinSize(minHeight = 90.dp),
        onClick = { expanded = !expanded }
    ) {
        Box(
            Modifier
                .padding(top = 5.dp)
                .fillMaxSize()
        ) {
            AmidTitle(
                titleString = requestDTO.type.value,
                modifier = Modifier
                    .padding(start = 15.dp)
                    .align(CenterStart)
                    .width(150.dp),
                fontSize = 20.sp,
                singleLine = false,
                textAlign = TextAlign.Center
            )
            AmidTitle(
                titleString = requestDTO.status.value,
                modifier = Modifier
                    .align(CenterEnd)
                    .width(150.dp),
                fontSize = 16.sp,
                color = MaterialTheme.colorScheme.onBackground,
                fontWeight = FontWeight.Black
            )
        }
        AnimatedVisibility(
            visible = expanded,
            enter = expandVertically(),
            exit = shrinkVertically()
        ) {
            Column {
                Param(
                    label = "Количество",
                    value = "${requestDTO.count} шт",
                )
                Param(
                    label = "Дата",
                    value = requestDTO.date.toHumanFormat(),
                )
                TextButton(
                    onClick = onDelete,
                    modifier = Modifier
                        .align(CenterHorizontally)
                        .padding(15.dp)
                ) {
                    AmidTitle(titleString = "Отменить заявку", fontSize = 18.sp)
                }
            }
        }
    }
}