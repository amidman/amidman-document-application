package com.example.amiddocapp.ui.navigation

import com.ramcosta.composedestinations.annotation.NavGraph


@NavGraph
annotation class RequestNavGraphs(
    val start:Boolean = false
)
