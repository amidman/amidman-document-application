package com.example.amiddocapp.ui.navigation

import com.example.amiddocapp.R
import com.example.amiddocapp.ui.screens.NavGraphs
import com.example.amiddocapp.ui.screens.destinations.RequestScreenDestination
import com.ramcosta.composedestinations.annotation.NavGraph
import com.ramcosta.composedestinations.spec.NavGraphSpec


@NavGraph
annotation class StudentBottomGraphs(
    val start:Boolean = false
)

object StudentBottomNavGraphs:BaseBottomNavGraphs{
    override val bottomGraphs: List<NavGraphData>
        get() = listOf(
            NavGraphData(
                direction = RequestScreenDestination,
                icon = R.drawable.student_request_icon,
                title = "Заявки"
            )
        )
    override val graphs: NavGraphSpec
        get() = NavGraphs.studentBottomGraphs

}