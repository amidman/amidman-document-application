package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.width
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.ui.common.AmidTitle
import com.example.amiddocapp.ui.common.BackButton

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectedEntityTopBar(
    title: String,
    titleFontSize:TextUnit = 24.sp,
    fontWeight: FontWeight = FontWeight.Bold,
    onBack: () -> Unit,
    actionButtons: @Composable RowScope.() -> Unit = {}
) {

    CenterAlignedTopAppBar(
        title = {
            AmidTitle(
                titleString = title,
                fontWeight = fontWeight,
                fontSize = titleFontSize,
                modifier = Modifier.width(300.dp)
            )
        },
        navigationIcon = {
            BackButton(onClick = onBack)
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer
        ),
        actions = actionButtons
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SelectedEntityTopBar(
    title: String,
    titleFontSize:TextUnit = 24.sp,
    fontWeight: FontWeight = FontWeight.Bold,
) {

    CenterAlignedTopAppBar(
        title = {
            AmidTitle(
                titleString = title,
                fontWeight = fontWeight,
                fontSize = titleFontSize,
                modifier = Modifier.width(300.dp)
            )
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer
        )
    )
}