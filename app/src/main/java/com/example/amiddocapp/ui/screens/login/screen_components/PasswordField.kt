package com.example.amiddocapp.ui.screens.login.screen_components


import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import com.example.amiddocapp.R
import com.example.amiddocapp.ui.screens.login.LoginViewModel


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PasswordField(
    modifier: Modifier,
    vm: LoginViewModel,
    isError:Boolean
) {

    val passwordHint = stringResource(id = R.string.password_hint)

    val password = vm.passwordFlow.collectAsState()

    var passwordVisibility by remember {
        mutableStateOf(false)
    }

    val iconResource = remember(passwordVisibility) {
        if (passwordVisibility) R.drawable.visibility_on else R.drawable.visibility_off
    }

    val visualTransformation = remember(passwordVisibility) {
        if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation()
    }

    OutlinedTextField(
        value = password.value,
        onValueChange = vm::changePassword,
        modifier = modifier,
        isError = isError,
        label = {
            Text(text = passwordHint)
        },
        singleLine = true,
        trailingIcon = {
            IconButton(onClick = { passwordVisibility = !passwordVisibility }) {
                Icon(painter = painterResource(id = iconResource), contentDescription = null)
            }
        },
        visualTransformation = visualTransformation
    )

}


