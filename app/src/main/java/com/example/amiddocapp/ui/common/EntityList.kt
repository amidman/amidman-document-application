package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import kotlin.math.exp

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun <T> EntityList(
    modifier: Modifier = Modifier,
    entityList: List<T>,
    isLoading: Boolean,
    isError: Boolean,
    onRefresh: () -> Unit,
    setNoError: () -> Unit = {},
    errorMessage:String?,
    expandFrom:Alignment.Vertical = Alignment.Bottom,
    shrinkTowards:Alignment.Vertical = Alignment.Top,
    errorAlignment: Alignment = Alignment.BottomCenter,
    entityItem: @Composable LazyItemScope.(index: Int, entity: T) -> Unit
) {
    val scrollState = rememberScrollState()
    val pullRefreshState = rememberPullRefreshState(refreshing = isLoading, onRefresh = onRefresh)
    Box(
        modifier = Modifier
            .fillMaxSize()
            .pullRefresh(pullRefreshState)
    ) {
        when {
            entityList.isEmpty() && !isError -> Box(modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollState)
            ) {
                AmidTitle(
                    titleString = "Пусто",
                    modifier = Modifier.align(Alignment.Center)
                )
            }
            else -> LazyColumn(modifier = modifier.fillMaxSize()) {
                itemsIndexed(entityList) { index, entity ->
                    entityItem(this, index, entity)
                }
            }
        }
        AnimatedErrorTitle(
            modifier = modifier.align(errorAlignment),
            isError = isError,
            setNoError = setNoError,
            errorMessage = errorMessage,
            expandFrom = expandFrom,
            shrinkTowards = shrinkTowards,
        )


        PullRefreshIndicator(
            refreshing = isLoading,
            state = pullRefreshState,
            modifier = Modifier.align(Alignment.TopCenter),
            contentColor = MaterialTheme.colorScheme.primary
        )

    }

}