package com.example.pgk_forum_app.ui.dialogs


import android.widget.Space
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.staggeredgrid.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.toLowerCase
import androidx.compose.ui.unit.dp
import java.util.*

fun searchString(value: String, searchValue: String): Boolean {
    if (searchValue.isBlank()) return false
    return value.lowercase().trim().contains(searchValue.trim().lowercase())
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun DialogDropDownField(
    title: String,
    itemList: List<String>,
    onValueChange: (value: String) -> Unit,
    value: String,
) {
    var expanded by remember { mutableStateOf(false) }
    val fieldModifier = Modifier
        .width(350.dp)
        .padding(top = 6.dp, start = 20.dp, end = 20.dp)
    val listModifier = Modifier
        .width(350.dp)
        .height(100.dp)
        .padding(top = 5.dp, bottom = 10.dp, start = 20.dp, end = 20.dp)
    DialogParamField(
        title = title,
        modifier = fieldModifier,
        value = value,
        onValueChange = {
            onValueChange(it);
            expanded = it.isNotEmpty()
        }
    )
    if (expanded)
        LazyColumn(
            modifier = listModifier,
            contentPadding = PaddingValues(5.dp)
        ){
            itemsIndexed(itemList.filter { searchString(it,value) }){index, item ->
                OutlinedButton(
                    modifier = Modifier
                        .height(40.dp),
                    onClick = { onValueChange(item);expanded = false },
                    content = { Text(text = item) }
                )
            }
        }
    else
        Spacer(modifier = listModifier)
 


}

