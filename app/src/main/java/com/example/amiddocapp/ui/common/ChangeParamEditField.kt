package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.dialogs.DialogDropDownField
import com.example.pgk_forum_app.ui.dialogs.DialogParamField


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChangeParamEditField(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(70.dp)
        .padding(horizontal = 25.dp, vertical = 5.dp),
    paramValue:String,
    paramTitle:String, // value of item title and hint in dialog field
    onValueChange:(value:String) -> Unit, // call when typing text in field
    isEntityField:Boolean = false, // if value of field depends of any entity enable this parameter
    entityStringList:List<String> = listOf(), // list of available entity names which we can select if isEntity parameter enabled
    singleLine:Boolean = true, // Change dialog field is single line
    fieldModifier:Modifier = Modifier
        .width(350.dp)
        .sizeIn(maxHeight = 350.dp)
        .padding(vertical = 6.dp, horizontal = 20.dp)
) {
    AmidTitle(
        titleString = paramTitle,
        modifier = Modifier.padding(start = 25.dp, top = 10.dp),
        fontSize = 20.sp
    )

    if (isEntityField)
        DialogDropDownField(
            title = paramTitle,
            itemList = entityStringList,
            onValueChange = onValueChange,
            value = paramValue,
        )
    else
        DialogParamField(
            title = paramTitle,
            value = paramValue,
            onValueChange = onValueChange,
            singleLine = singleLine,
            modifier = fieldModifier
        )


}