package com.example.amiddocapp.ui.screens.student_request.screen_components

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CardElevation
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterEnd
import androidx.compose.ui.Alignment.Companion.CenterStart
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.domain.model.student_request.StudentRequestType
import com.example.amiddocapp.ui.common.AmidTitle
import com.example.amiddocapp.ui.common.DropDownSelector
import com.example.amiddocapp.ui.screens.student_request.RequestViewModel
import com.example.amiddocapp.ui.util.collectFlow
import kotlinx.coroutines.flow.onEach

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateRequestButton(requestViewModel: RequestViewModel) {

    var enabled by remember {
        mutableStateOf(false)
    }

    var count by remember {
        mutableStateOf(1)
    }

    var type by remember {
        mutableStateOf(StudentRequestType.STUDYDOCUMENT)
    }

    requestViewModel.isCreateButtonEnabled.onEach {
        enabled = it
    }.collectFlow()

    requestViewModel.docCount.onEach {
        count = it
    }.collectFlow()

    requestViewModel.documentType.onEach {
        type = it
    }.collectFlow()

    val width by animateDpAsState(targetValue = if (enabled) 300.dp else 110.dp)
    val height by animateDpAsState(targetValue = if (enabled) 200.dp else 60.dp)

    val boxModifier = Modifier
        .padding(top = 20.dp)
        .fillMaxWidth()
    Card(
        modifier = Modifier
            .size(
                width = width,
                height = height
            ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 5.dp
        )
    ) {
        if (enabled) {
            Box(modifier = boxModifier) {
                AmidTitle(
                    titleString = "Количество",
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .padding(10.dp),
                    fontSize = 16.sp
                )
                DropDownSelector(
                    modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .padding(10.dp),
                    style = TextStyle(
                        fontSize = 16.sp
                    ),
                    value = "$count шт",
                    valueList = (1..3).toList().map { it.toString() },
                    onValueChange = requestViewModel::changeDocCount
                )
            }
            Box(modifier = boxModifier) {

                AmidTitle(
                    titleString = "Тип",
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .padding(10.dp),
                    fontSize = 16.sp
                )
                DropDownSelector(
                    modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .padding(10.dp),
                    style = TextStyle(
                        fontSize = 16.sp
                    ),
                    value = type.value,
                    valueList = StudentRequestType.values().map { it.value },
                    onValueChange = requestViewModel::changeDocumentType
                )
            }
            Box(modifier = boxModifier) {
                Card(
                    onClick = requestViewModel::changeEnabled,
                    modifier = Modifier
                        .width(110.dp)
                        .align(CenterStart)
                ) {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        AmidTitle(
                            titleString = "Отмена",
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Medium,
                            modifier = Modifier
                                .align(Alignment.Center)
                                .padding(10.dp)
                        )
                    }
                }
                Card(
                    onClick = requestViewModel::createStudentRequest,
                    modifier = Modifier
                        .width(110.dp)
                        .align(CenterEnd)
                ) {
                    Box(modifier = Modifier.fillMaxWidth()) {
                        AmidTitle(
                            titleString = "Заказать",
                            fontSize = 16.sp,
                            fontWeight = FontWeight.Medium,
                            modifier = Modifier
                                .align(Alignment.Center)
                                .padding(10.dp)
                        )
                    }
                }
            }
        } else
            Card(onClick = requestViewModel::changeEnabled) {
                Box(modifier = Modifier.fillMaxSize()) {
                    AmidTitle(
                        titleString = "Заказать",
                        fontSize = 16.sp,
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .align(Alignment.Center)
                            .padding(10.dp),
                    )
                }
            }
    }

}