package com.example.amiddocapp.ui.common

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Bottom
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.Top
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.delay

@Composable
fun AnimatedErrorTitle(
    modifier: Modifier,
    isError:Boolean,
    setNoError: ()-> Unit,
    errorMessage:String?,
    expandFrom:Alignment.Vertical = Bottom,
    shrinkTowards:Alignment.Vertical = Top,
) {
    LaunchedEffect(isError){
        delay(4000)
        setNoError()
    }
    AnimatedVisibility(
        modifier = modifier,
        visible = isError,
        enter = expandVertically(expandFrom = expandFrom,),
        exit = shrinkVertically(shrinkTowards = shrinkTowards,),
    ) {
        Card(modifier = Modifier.padding(20.dp).fillMaxWidth(), colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.errorContainer
        )){
            Text(
                text = errorMessage ?: "Произошла ошибка",
                modifier = Modifier
                    .align(CenterHorizontally)
                    .padding(10.dp),
                color = MaterialTheme.colorScheme.onErrorContainer,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center
            )
        }
    }
}