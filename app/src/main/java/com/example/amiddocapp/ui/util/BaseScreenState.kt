package com.example.amiddocapp.ui.util

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow

sealed class BaseScreenState(val error:String? = null,val navDestination:String? = null){
    object Nothing: BaseScreenState()
    object Success: BaseScreenState()
    object Loading: BaseScreenState()
    class Error(error: String? = "Server Error Try Again"): BaseScreenState(error = error)
    class NavEvent(navDestination: String?): BaseScreenState(navDestination=navDestination)
}

suspend fun MutableStateFlow<BaseScreenState>.loading(){
    emit(BaseScreenState.Loading)
}

suspend fun MutableStateFlow<BaseScreenState>.error(error: String){
    emit(BaseScreenState.Error(error))
}

suspend fun MutableStateFlow<BaseScreenState>.nothing(){
    emit(BaseScreenState.Nothing)
}
