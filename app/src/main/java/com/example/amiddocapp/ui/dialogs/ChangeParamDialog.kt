package com.example.amiddocapp.ui.dialogs

import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.ui.common.AmidTitle
import com.example.pgk_forum_app.ui.dialogs.DialogDropDownField
import com.example.pgk_forum_app.ui.dialogs.DialogErrorTitle
import com.example.pgk_forum_app.ui.dialogs.DialogParamField


@Composable
fun ChangeParamWindow(
    onDismiss: () -> Unit,
    onAdd: (newValue: String) -> Unit,
    isError: Boolean,
    errorMessage: String?,
    title: String = "Изменение Параметра",
    paramTitle: String,
    paramValue: String,
    isEntityField: Boolean = false,
    entityStringList: List<String> = listOf(),
    singleLine:Boolean = true,
    dialogFieldModifier: Modifier? = null,
) {

    val fieldModifier = dialogFieldModifier ?: Modifier
        .width(350.dp)
        .sizeIn(maxHeight = 350.dp)
        .padding(vertical = 6.dp, horizontal = 20.dp)
    var param: String by remember {
        mutableStateOf(paramValue)
    }
    AlertDialog(
        onDismissRequest = {},
        buttons = {
            Box() {
                Column() {
                    AmidTitle(
                        titleString = title,
                        modifier = Modifier.align(Alignment.CenterHorizontally),
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                    if (isEntityField)
                        DialogDropDownField(
                            title = paramTitle,
                            itemList = entityStringList,
                            onValueChange = { param = it },
                            value = param,
                        )
                    else
                        DialogParamField(
                            title = paramTitle,
                            value = param,
                            onValueChange = { param = it },
                            singleLine = singleLine,
                            modifier = fieldModifier
                        )

                    DialogErrorTitle(
                        errorMessage = errorMessage,
                        isError = isError,
                        modifier = Modifier
                            .height(120.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                }
                AddButtons(
                    onAdd = {
                        onAdd(param)
                    },
                    onDismiss = onDismiss,
                    confirmButtonText = "Изменить"
                )
            }
        },

        )
}