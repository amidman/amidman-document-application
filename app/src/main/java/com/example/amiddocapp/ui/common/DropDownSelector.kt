package com.example.amiddocapp.ui.common

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp


@Composable
fun DropDownSelector(
    modifier: Modifier = Modifier,
    value:String,
    valueList:List<String>,
    onValueChange:(value:String) -> Unit,
    style:TextStyle = TextStyle (
        color = Color.Unspecified,
        fontSize = 24.sp,
        textAlign = TextAlign.Start,
        fontWeight = FontWeight.Bold,
        shadow = Shadow(),
    )
) {
    var expanded by remember { mutableStateOf(false) }

    Box(modifier = modifier){
        AmidTitle(
            titleString = value,
            fontSize = style.fontSize,
            fontWeight = style.fontWeight ?: FontWeight.Bold,
            color = style.color,
            textAlign = style.textAlign ?: TextAlign.Start,
            singleLine = true,
            modifier = Modifier
                .clickable { expanded = true }
        )
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            valueList.forEach{ value ->
                DropdownMenuItem(onClick = {
                    onValueChange(value)
                    expanded = false
                }) {
                    Text(text = value)
                }
            }
        }
    }
}

//TextStyle (
//color = MaterialTheme.colorScheme.primary,
//fontSize = 24.sp,
//textAlign = TextAlign.Start,
//fontWeight = FontWeight.Bold,
//shadow = Shadow(),
//)