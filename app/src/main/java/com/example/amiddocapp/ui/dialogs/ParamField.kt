package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ParamField(
    title:String,
    modifier: Modifier,
    value:String,
    onValueChange:(value:String) -> Unit,
    singleLine:Boolean = true
) {
    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier,
        singleLine = singleLine,
        label = {
            Text(text = title)
        }
    )
}