package com.example.amiddocapp.ui.screens.student_request

import androidx.compose.runtime.Composable
import com.example.amiddocapp.ui.navigation.StudentBottomGraphs
import com.example.amiddocapp.ui.screens.NavGraphs
import com.example.amiddocapp.ui.screens.destinations.StudentRequestListDestination
import com.example.amiddocapp.ui.screens.student_request.screen_components.StudentRequestList
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@StudentBottomGraphs(start = true)
@Destination
@Composable
fun RequestScreen(
    navigator: DestinationsNavigator,
    requestViewModel: RequestViewModel
) {

    DestinationsNavHost(navGraph = NavGraphs.requests) {

        composable(StudentRequestListDestination) {
            StudentRequestList(
                requestViewModel = requestViewModel,
                navigator = destinationsNavigator
            )
        }

    }
}