package com.example.amiddocapp.ui.navigation

import com.ramcosta.composedestinations.spec.DirectionDestinationSpec
import com.ramcosta.composedestinations.spec.NavGraphSpec

data class NavGraphData(
    val direction:DirectionDestinationSpec,
    val icon:Int,
    val title: String,
)

interface BaseBottomNavGraphs {
    val bottomGraphs:List<NavGraphData>
    val graphs:NavGraphSpec
}

