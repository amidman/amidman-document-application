package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.dialogs.searchString

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChangeParamField(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(70.dp)
        .padding(horizontal = 25.dp, vertical = 5.dp),
    paramValue: String, // current value
    paramTitle:String, // value of item title and hint in dialog field
    onClick:() -> Unit = {}, // call when we click on item
    onChange:(value:String) -> Unit = {}, // call when typing text in dialog field
    entityStringList:List<String> = listOf(), // list of available entity names which we can select if isEntity parameter enabled
    isEditMode:Boolean = true, // if true show edit icon which provide change and delete parameters
    singleLine:Boolean = true, // Change dialog field is single line
    fieldContainerModifier:Modifier = Modifier.fillMaxSize() // modifier of box in card
) {

    var isEditState by remember {
        mutableStateOf(false)
    }
    var editValue by remember {
        mutableStateOf(paramValue)
    }
    val focusRequester = FocusRequester()
    AmidTitle(
        titleString = paramTitle,
        modifier = Modifier.padding(start = 25.dp, top = 10.dp),
        fontSize = 20.sp
    )
    Card(
        modifier = modifier,
        onClick = onClick,
    ) {
        Box(modifier = fieldContainerModifier){
            PanelEditTextField(
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .padding(start = 40.dp, top = 10.dp, bottom = 10.dp)
                    .width(240.dp),
                value = editValue,
                onValueChange = {
                    editValue = it
                },
                isEnabled = isEditState,
                focusRequester = focusRequester,
                singleLine = singleLine
            )
            if (isEditMode && !isEditState)
                ChangeIcon(
                    onChange = {
                        isEditState = true
                        focusRequester.requestFocus()
                    },
                    modifier = Modifier
                        .padding(end = 20.dp)
                        .align(Alignment.CenterEnd)
                )
            if (isEditMode && isEditState)
                DoneIcon(
                    onChange = {
                        isEditState = false
                        editValue = editValue.trim()
                        if (paramValue != editValue)
                            onChange(editValue)

                    },
                    modifier = Modifier
                        .padding(end = 20.dp)
                        .align(Alignment.CenterEnd)
                )
        }
    }
    if (isEditMode && isEditState && entityStringList.isNotEmpty())
        LazyColumn(
            modifier = Modifier.fillMaxWidth().height(200.dp),
            contentPadding = PaddingValues(5.dp)
        ){
            itemsIndexed(entityStringList.filter { searchString(it,editValue) }){index, item ->
                OutlinedButton(
                    modifier = Modifier
                        .height(40.dp),
                    onClick = { editValue = item },
                    content = { Text(text = item) }
                )
            }
        }
}

@Composable
private fun PanelEditTextField(
    modifier: Modifier,
    value:String,
    onValueChange:(value:String) -> Unit,
    isEnabled:Boolean,
    focusRequester: FocusRequester,
    singleLine:Boolean
) {


    BasicTextField(
        modifier = modifier.focusRequester(focusRequester),
        value = value,
        onValueChange = onValueChange,
        readOnly = !isEnabled,
        textStyle = TextStyle(
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            textAlign = TextAlign.Start,
            color = MaterialTheme.colorScheme.primary
        ),
        singleLine = singleLine,

    )
}

