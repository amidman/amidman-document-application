package com.example.amiddocapp.ui.screens.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.amiddocapp.domain.repository.LoginRepository
import com.example.amiddocapp.domain.utils.ROLES
import com.example.amiddocapp.ui.screens.destinations.StudentScreenDestination
import com.example.amiddocapp.ui.util.ViewModelHandle
import com.example.amiddocapp.ui.util.requestStatusHandle
import com.example.pgk_forum_app.domain.model.login.LoginData
import com.example.pgk_forum_app.domain.model.login.TokenBody
import com.example.amiddocapp.ui.util.BaseScreenState
import com.example.amiddocapp.ui.util.loading
import com.example.amiddocapp.ui.util.nothing
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class LoginViewModel(
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _state: MutableStateFlow<BaseScreenState> =
        MutableStateFlow(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    private val _loginFlow = MutableStateFlow("ВоронковДА")
    val loginFlow = _loginFlow.asStateFlow()

    fun changeLogin(newValue: String) {
        viewModelScope.launch {
            _loginFlow.emit(newValue)
            _state.nothing()
        }
    }

    private val _passwordFlow = MutableStateFlow("ВоронковДА")
    val passwordFlow = _passwordFlow.asStateFlow()

    fun changePassword(newValue: String) {
        viewModelScope.launch {
            _passwordFlow.emit(newValue)
            _state.nothing()
        }
    }

    fun login() {
        viewModelScope.launch {
            _state.loading()
            val loginData = LoginData(
                login = loginFlow.value,
                password = passwordFlow.value
            )
            val requestData = loginRepository.login(loginData)
            val viewModelHandle = ViewModelHandle<TokenBody> { tokenBody ->
                if (tokenBody.role != ROLES.STUDENT.name) {
                    _state.emit(BaseScreenState.Error("Авторизация возможна только для студентов"))
                    return@ViewModelHandle
                }
                _state.emit(BaseScreenState.NavEvent(StudentScreenDestination.route))
            }
            requestData.requestStatusHandle(_state, viewModelHandle)
        }
    }
}