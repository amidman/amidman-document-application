package com.example.amiddocapp.ui.screens.login.screen_components

import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.example.amiddocapp.R
import com.example.amiddocapp.ui.screens.login.LoginViewModel

@Composable
fun LoginButton(
    modifier: Modifier,
    vm: LoginViewModel
) {
    val buttonTitle = stringResource(id = R.string.login_button_title)

    OutlinedButton(
        onClick = vm::login,
        modifier = modifier,
    ) {
        Text(text = buttonTitle)
    }
}