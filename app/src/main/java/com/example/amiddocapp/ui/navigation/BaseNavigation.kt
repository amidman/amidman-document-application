package com.example.amiddocapp.ui.navigation

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import okhttp3.Dispatcher

open class BaseNavigation<T> {
    private val _navFlow = MutableSharedFlow<T>(replay = 2)
    val navFlow = _navFlow.asSharedFlow()

    fun navToDestination(destination: T) = CoroutineScope(Dispatchers.IO).launch{
        _navFlow.emit(destination)
    }

    fun navToPrevDestination() = CoroutineScope(Dispatchers.IO).launch{
        _navFlow.emit(_navFlow.replayCache.first())
    }
}