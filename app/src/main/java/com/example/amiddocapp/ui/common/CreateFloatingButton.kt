package com.example.amiddocapp.ui.common

import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import com.example.amiddocapp.R

@Composable
fun CreateFloatingButton(
    onClick:() -> Unit
) {
    val plusIconPainter = painterResource(id = R.drawable.plus_icon)
    FloatingActionButton(onClick = onClick) {
        Icon(painter = plusIconPainter, contentDescription = null)
    }
}