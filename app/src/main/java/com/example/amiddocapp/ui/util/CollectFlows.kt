package com.example.amiddocapp.ui.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@Composable
fun collectFlows(onCollect : suspend CoroutineScope.() -> Unit){
    val lifecycleOwner = LocalLifecycleOwner.current
    lifecycleOwner.lifecycleScope.launch {
        lifecycleOwner.repeatOnLifecycle(
            Lifecycle.State.STARTED,
            block = onCollect
        )
    }
}

@Composable
fun <T> Flow<T>.collectFlow(state:Lifecycle.State = Lifecycle.State.STARTED){
    val lifecycleOwner = LocalLifecycleOwner.current
    lifecycleOwner.lifecycleScope.launch {
        lifecycleOwner.repeatOnLifecycle(
            state,
        ){
            this@collectFlow.collect()
        }
    }
}