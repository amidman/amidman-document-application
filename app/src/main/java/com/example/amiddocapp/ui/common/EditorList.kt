package com.example.amiddocapp.ui.common

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier


@Composable
fun <T> EditorList(
    modifier: Modifier = Modifier,
    entityList: List<T>,
    isLoading: Boolean,
    titleString: (value: T) -> String,
    onClick: (value: T) -> Unit,
    onDelete: (value: T) -> Unit = {},
    isEditMode: Boolean = true,
    isError:Boolean,
    errorMessage:String?,
    onRefresh:() -> Unit = {},
) {

    EntityList(
        entityList = entityList,
        isLoading = isLoading,
        modifier = modifier,
        onRefresh = onRefresh,
        isError = isError,
        errorMessage = errorMessage
    ) { index, entity ->
        EntityListItem(
            onDelete = { onDelete(entity) },
            onClick = { onClick(entity) },
            titleString = titleString(entity),
            isEditMode = isEditMode,
        )
    }

}