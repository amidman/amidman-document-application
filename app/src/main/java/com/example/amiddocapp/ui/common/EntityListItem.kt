package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.ui.common.DeleteIcon
import com.example.amiddocapp.ui.common.AmidTitle


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EntityListItem(
    onDelete: () -> Unit = {},
    onClick: () -> Unit = {},
    modifier:Modifier = Modifier
        .fillMaxWidth()
        .height(90.dp)
        .padding(horizontal = 10.dp, vertical = 5.dp),
    titleString: String,
    isEditMode:Boolean = true,
) {
    Card(
        modifier = modifier,
        onClick = onClick,
    ) {
        Box(modifier = Modifier.fillMaxSize()){
            AmidTitle(
                modifier = Modifier
                    .padding(start = 40.dp)
                    .align(Alignment.CenterStart)
                    .width(240.dp)
                ,
                titleString = titleString,
                fontSize = 20.sp,
            )
            if (isEditMode)
            DeleteIcon(
                onDelete = onDelete,
                modifier = Modifier
                    .padding(end = 20.dp)
                    .align(Alignment.CenterEnd)
            )
        }
    }
}