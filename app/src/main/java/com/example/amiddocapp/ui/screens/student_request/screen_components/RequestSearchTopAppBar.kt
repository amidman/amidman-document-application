package com.example.amiddocapp.ui.screens.student_request.screen_components

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.res.painterResource
import com.example.amiddocapp.R
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import com.example.amiddocapp.ui.common.AmidTitle


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RequestSearchTopAppBar() {
    var isFilterVisible by remember {
        mutableStateOf(false)
    }
    Column{
        TopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(
                containerColor = MaterialTheme.colorScheme.secondaryContainer
            ),
            title = {
                AmidTitle(titleString = "Заявки", color = MaterialTheme.colorScheme.primary)
            },
            actions = {
                IconButton(onClick = { isFilterVisible = !isFilterVisible }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_baseline_filter_alt_24),
                        contentDescription = null
                    )
                }
            }
        )
    }
}