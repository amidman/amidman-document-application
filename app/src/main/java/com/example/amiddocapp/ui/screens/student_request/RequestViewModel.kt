package com.example.amiddocapp.ui.screens.student_request

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.amiddocapp.domain.model.student_request.RequestCreateModel
import com.example.amiddocapp.domain.model.student_request.StudentRequestDTO
import com.example.amiddocapp.domain.model.student_request.StudentRequestType
import com.example.amiddocapp.domain.model.student_request.toRequestType
import com.example.amiddocapp.domain.repository.StudentRequestRepository
import com.example.amiddocapp.ui.util.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.datetime.toKotlinLocalDate
import java.time.LocalDate

class RequestViewModel(
    private val requestRepository: StudentRequestRepository
) : ViewModel() {
    private fun List<StudentRequestDTO>.sortByDate():List<StudentRequestDTO> =
        sortedBy { studentRequestDTO -> studentRequestDTO.date }

    private val _state = MutableStateFlow<BaseScreenState>(BaseScreenState.Nothing)
    val state = _state.asStateFlow()

    fun nothing() = viewModelScope.launch { _state.nothing() }

    private val _requestList = MutableStateFlow<List<StudentRequestDTO>>(listOf())
    val requestList = _requestList.asStateFlow()

    private val _isCreateButtonEnabled = MutableStateFlow(false)
    val isCreateButtonEnabled = _isCreateButtonEnabled.asStateFlow()

    fun changeEnabled() {
        viewModelScope.launch {
            _isCreateButtonEnabled.emit(!_isCreateButtonEnabled.value)
        }
    }

    private val _docCount = MutableStateFlow(1)
    val docCount = _docCount.asStateFlow()

    fun changeDocCount(newValue: String) {
        viewModelScope.launch {
            _docCount.emit(newValue.toInt())
        }
    }

    private val _documentType =
        MutableStateFlow<StudentRequestType>(StudentRequestType.STUDYDOCUMENT)
    val documentType = _documentType.asStateFlow()

    fun changeDocumentType(newValue: String) {
        viewModelScope.launch {
            val value = newValue.toRequestType()
            value?.let {
                _documentType.emit(value)
            }
        }
    }

    init {
        getStudentRequests()
    }


    fun getStudentRequests() {
        viewModelScope.launch {
            _state.loading()
            val requestData = requestRepository.studentRequests()
            requestData.requestStatusHandle(state = _state, dataFlow = _requestList)
            _requestList.emit(_requestList.value.sortByDate())
        }
    }

    fun createStudentRequest() {
        viewModelScope.launch {
            val currentDate = LocalDate.now().toKotlinLocalDate()
            val requestCreateModel = RequestCreateModel(
                date = currentDate,
                type = _documentType.value,
                count = _docCount.value
            )
            val requestData = requestRepository.createStudentRequest(requestCreateModel)
            val viewModelHandle =
                ViewModelHandle<StudentRequestDTO> { requestDTO ->
                    val newListValue =
                        (_requestList.value + requestDTO).sortByDate()
                    _requestList.emit(newListValue)
                    _isCreateButtonEnabled.emit(false)
                }
            requestData.requestStatusHandle(_state, viewModelHandle)
        }
    }

    fun declineRequest(studentRequestId:Int){
        viewModelScope.launch {
            val requestData = requestRepository.declineStudentRequest(studentRequestId)
            val viewModelHandle = ViewModelHandle<Unit> {
                val requestList = _requestList.value
                val declinedStudentRequest = requestList.find { req -> req.id == studentRequestId }
                if (declinedStudentRequest != null)
                    _requestList.emit((requestList - declinedStudentRequest).sortByDate())
            }
            requestData.requestStatusHandle(_state,viewModelHandle)
        }
    }

}