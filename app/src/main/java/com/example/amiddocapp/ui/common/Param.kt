package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.screens.login.screen_components.MiniTitle

@Composable
fun ColumnScope.Param(label:String,value:String?) {
    Card(
        modifier = Modifier
            .padding(start = 10.dp, top = 10.dp)
            .fillMaxWidth()
            .height(70.dp),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.secondaryContainer
        )
    ) {
        Box(modifier = Modifier
            .fillMaxSize()
            .align(Alignment.Start),
        ){
            AmidTitle(
                titleString = label,
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier
                    .padding(start = 20.dp)
                    .align(Alignment.CenterStart),
                fontSize = 16.sp
            )
            AmidTitle(
                titleString = if (value.isNullOrEmpty()) "NOT_DEFINED" else value,
                color = MaterialTheme.colorScheme.primary,
                modifier = Modifier
                    .padding(end = 40.dp)
                    .align(Alignment.CenterEnd),
                fontSize = 16.sp
            )
        }
    }
}