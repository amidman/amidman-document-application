package com.example.amiddocapp.ui.screens.student

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.example.amiddocapp.ui.common.EntityBottomNavigation
import com.example.amiddocapp.ui.navigation.StudentBottomNavGraphs
import com.example.amiddocapp.ui.screens.NavGraphs
import com.example.amiddocapp.ui.screens.destinations.RequestScreenDestination
import com.example.amiddocapp.ui.screens.student_request.RequestScreen
import com.example.amiddocapp.ui.screens.student_request.RequestViewModel
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import org.koin.androidx.compose.getViewModel


@OptIn(ExperimentalMaterial3Api::class)
@RootNavGraph
@Destination
@Composable
fun StudentScreen(
    navigator: DestinationsNavigator
) {
    val requestViewModel: RequestViewModel = getViewModel()
    val studentBottomNavigation = rememberNavController()
    Scaffold(
        bottomBar = {
            EntityBottomNavigation(
                navController = studentBottomNavigation,
                navGraphs = StudentBottomNavGraphs
            )
        },
    ) {
        DestinationsNavHost(
            navGraph = NavGraphs.studentBottomGraphs,
            navController = studentBottomNavigation,
            modifier = Modifier.padding(it)
        ) {
            composable(RequestScreenDestination) {
                RequestScreen(
                    navigator = destinationsNavigator,
                    requestViewModel = requestViewModel
                )
            }
        }
    }
}