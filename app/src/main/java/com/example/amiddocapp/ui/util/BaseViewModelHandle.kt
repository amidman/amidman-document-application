package com.example.amiddocapp.ui.util


import com.example.amiddocapp.domain.model.network_base.ErrorModel
import com.example.amiddocapp.domain.model.network_base.RequestStatus
import kotlinx.coroutines.flow.MutableStateFlow


fun interface ViewModelHandle<T>{
    suspend fun onSuccess(data:T)
    suspend fun onError(errorModel: ErrorModel?){}
}




suspend fun <T> RequestStatus<T>?.requestStatusHandle(state: MutableStateFlow<BaseScreenState>, dataFlow: MutableStateFlow<T>){
    if (this == null)
        return
    when(this){
        is RequestStatus.Error -> {
            val error = this.error?.description ?: ""
            state.error(error)
        }
        is RequestStatus.Success -> {
            state.emit(BaseScreenState.Success)
            this.data?.let {
                dataFlow.emit(it)
            }
        }

    }
}

suspend fun <T> RequestStatus<T>?.requestStatusHandle(state: MutableStateFlow<BaseScreenState>){
    if (this == null)
        return
    when(this){
        is RequestStatus.Error -> {
            val error = this.error?.description ?: ""
            state.error(error)
        }
        is RequestStatus.Success -> {
            state.emit(BaseScreenState.Success)
        }
    }
}



suspend fun <T> RequestStatus<T>?.requestStatusHandle(
    state: MutableStateFlow<BaseScreenState>,
    viewModelHandle: ViewModelHandle<T>
){
    if (this == null)
        return
    when(this){
        is RequestStatus.Error -> {
            val error = this.error?.description ?: ""
            state.error(error)
            viewModelHandle.onError(errorModel = this.error)
        }
        is RequestStatus.Success -> {
            state.emit(BaseScreenState.Success)
            data?.let { viewModelHandle.onSuccess(data = this.data) }
        }

    }
}
