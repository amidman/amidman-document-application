package com.example.amiddocapp.ui.common

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.amiddocapp.ui.navigation.BaseBottomNavGraphs
import com.example.amiddocapp.ui.screens.NavGraphs
import com.example.amiddocapp.ui.screens.appCurrentDestinationAsState
import com.ramcosta.composedestinations.navigation.navigate
import com.ramcosta.composedestinations.navigation.popUpTo
import com.ramcosta.composedestinations.utils.startDestination

@Composable
fun EntityBottomNavigation(
    navController: NavController,
    navGraphs: BaseBottomNavGraphs,
) {

    val startDirection = navGraphs.graphs.startDestination
    val currentDestination = navController.appCurrentDestinationAsState().value ?: startDirection
    NavigationBar(
        modifier = Modifier.height(75.dp),
    ) {
        val values = navGraphs.bottomGraphs
        values.forEach { navGraph ->
            val title = navGraph.title
            val icon = painterResource(id = navGraph.icon)
            NavigationBarItem(
                selected = navGraph.direction == currentDestination,
                onClick = { navController.navigate(navGraph.direction){
                    popUpTo(startDirection){
                        saveState = true
                    }
                    restoreState = true
                    launchSingleTop = true
                } },
                icon = {
                    Icon(
                        painter = icon,
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.size(24.dp)
                    )
                },
                label = {
                    Text(text = title)
                },
            )
        }
    }

}