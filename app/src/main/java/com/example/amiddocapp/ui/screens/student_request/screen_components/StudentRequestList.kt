package com.example.amiddocapp.ui.screens.student_request.screen_components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.amiddocapp.domain.model.student_request.StudentRequestDTO
import com.example.amiddocapp.ui.common.AnimatedErrorTitle
import com.example.amiddocapp.ui.common.EntityList
import com.example.amiddocapp.ui.navigation.RequestNavGraphs
import com.example.amiddocapp.ui.screens.student_request.RequestViewModel
import com.example.amiddocapp.ui.util.collectFlow
import com.example.amiddocapp.ui.util.BaseScreenState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.flow.onEach



@RequestNavGraphs(start = true)
@Destination
@Composable
fun StudentRequestList(
    requestViewModel: RequestViewModel,
    navigator:DestinationsNavigator
) {

    var isLoading by remember {
        mutableStateOf(false)
    }
    var isError by remember {
        mutableStateOf(false)
    }
    var errorMessage: String? by remember {
        mutableStateOf(null)
    }
    var requestList by remember {
        mutableStateOf(listOf<StudentRequestDTO>())
    }

    requestViewModel.requestList.onEach {
        requestList = it
    }.collectFlow()

    requestViewModel.state.onEach { state ->
        isLoading = state is BaseScreenState.Loading
        isError = state is BaseScreenState.Error
        errorMessage = state.error
    }.collectFlow()

    Scaffold(
        topBar = {
            RequestSearchTopAppBar()
        },
        floatingActionButton = {
            CreateRequestButton(requestViewModel = requestViewModel)
        }
    ) { paddingValues ->
        Box(modifier = Modifier.padding(paddingValues)){
            EntityList(
                entityList = requestList,
                isLoading = isLoading,
                isError = isError,
                errorMessage = errorMessage,
                onRefresh = requestViewModel::getStudentRequests,
                setNoError = requestViewModel::nothing,
                expandFrom = Alignment.Top,
                shrinkTowards = Alignment.Bottom,
                errorAlignment = Alignment.TopCenter
            ) { index: Int, requestDTO: StudentRequestDTO ->
                StudentRequestListItem(
                    requestDTO = requestDTO,
                    onDelete = {
                        requestViewModel.declineRequest(requestDTO.id ?: 0)
                    }
                )
            }
        }
    }
}