package com.example.amiddocapp.ui.common


import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pgk_forum_app.ui.dialogs.ParamListDialogAddWindow


@Composable
fun ParamEmptyItems(emptyItemsTitle:String) {
    val modifier = Modifier
        .fillMaxWidth()
        .height(70.dp)
        .padding(vertical = 5.dp)
    Card(modifier = modifier){
        Box(modifier = Modifier.fillMaxSize()){
            AmidTitle(titleString = emptyItemsTitle, modifier = Modifier.align(Center), fontSize = 18.sp)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ParamListItem(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .height(70.dp)
        .padding(vertical = 5.dp),
    paramValue:String,
    onDelete:()->Unit = {},
    onClick:()->Unit = {},
    isEditMode:Boolean
) {
    Card(
        modifier = modifier,
        onClick = onClick
    ) {
        Box(modifier = Modifier.fillMaxSize()){
            AmidTitle(
                modifier = Modifier
                    .padding(start = 40.dp)
                    .align(Alignment.CenterStart),
                titleString = paramValue,
                fontSize = 20.sp,
            )
            if (isEditMode)
            DeleteIcon(
                onDelete = onDelete,
                modifier = Modifier
                    .padding(end = 20.dp)
                    .align(Alignment.CenterEnd)
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun <T> ColumnScope.ChangeParamList(
    paramTitle:String, // value of item title and hint in dialog field
    itemList:List<T>, // current item list value, provide callback with item value
    itemListString:List<String>, // item list but map to list of string
    emptyItemTitle:String, // title if itemList is empty
    valueTitle:String = "Введите параметр", // value of hint in create dialog
    onDelete: (value:T) -> Unit = {}, // call when click on delete icon
    onAdd:(value:String) -> Unit = {}, // call when click on text button with add title
    onItemClick:(value:T) -> Unit = {}, // call when click on item
    isCreateDialogEnabled:Boolean = false, // if true show create dialog
    setCreateDialog:(value:Boolean) -> Unit = {},
    isError:Boolean = false,
    errorString:String? = null,
    allItemList:List<String> = listOf(), // list of available entity names which we can select if isEntity parameter enabled
    isEditMode:Boolean = true, // if true show edit icons which provide change and delete of param
) {


    Box(Modifier.fillMaxWidth()){
        AmidTitle(
            titleString = paramTitle,
            modifier = Modifier
                .align(Alignment.CenterStart)
                .padding(start = 25.dp, top = 10.dp),
            fontSize = 20.sp
        )
        if (isEditMode)
        TextButton(onClick = { setCreateDialog(true) }, modifier = Modifier
            .align(Alignment.CenterEnd)
            .padding(end = 10.dp, top = 10.dp)) {
            AmidTitle(titleString = "Добавить", fontSize = 20.sp)
        }
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .sizeIn(minHeight = 170.dp)
            .padding(horizontal = 25.dp, vertical = 5.dp),
    ){
        if (itemList.isEmpty() || itemList.size != itemListString.size)
            item{ ParamEmptyItems(emptyItemTitle) }
        else
            itemsIndexed(itemList){index: Int, item: T ->
                ParamListItem(
                    paramValue = itemListString[index],
                    onDelete = {onDelete(item)},
                    onClick = {onItemClick(item)},
                    isEditMode = isEditMode,
                )
            }
    }
    

    if (isCreateDialogEnabled && isEditMode)
        ParamListDialogAddWindow(
            onDismiss = { setCreateDialog(false) },
            onAdd = onAdd,
            isError = isError,
            errorMessage = errorString,
            valueTitle = valueTitle,
            isEntityAddWindow = true,
            allEntityList = allItemList
        )
    

    

}