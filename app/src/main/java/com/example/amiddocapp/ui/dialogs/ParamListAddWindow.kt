package com.example.pgk_forum_app.ui.dialogs

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.material.AlertDialog
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.ui.common.AmidTitle
import com.example.amiddocapp.ui.dialogs.AddButtons

@Composable
fun ParamListDialogAddWindow(
    onDismiss: () -> Unit,
    onAdd: (value: String) -> Unit,
    isError: Boolean,
    errorMessage: String?,
    title: String = "Введите параметр для добавления",
    valueTitle: String,
    isEntityAddWindow: Boolean = false,
    allEntityList: List<String> = listOf()
) {


    var value: String by remember {
        mutableStateOf("")
    }

    AlertDialog(
        onDismissRequest = {},
        buttons = {
            Box(Modifier.sizeIn(minHeight = 250.dp)) {
                Column() {
                    AmidTitle(
                        titleString = title,
                        modifier = Modifier.align(CenterHorizontally),
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                    if (isEntityAddWindow)
                        DialogDropDownField(
                            title = valueTitle,
                            itemList = allEntityList,
                            onValueChange = { value = it },
                            value = value
                        )
                    else
                        DialogParamField(
                            title = valueTitle,
                            value = value,
                            onValueChange = { value = it }
                        )

                    DialogErrorTitle(
                        errorMessage = errorMessage,
                        isError = isError,
                        modifier = Modifier
                            .height(120.dp)
                            .align(Alignment.CenterHorizontally)
                    )

                }
                AddButtons(
                    onAdd = {
                        onAdd(value)
                    },
                    onDismiss = onDismiss
                )
            }
        },

        )
}