package com.example.amiddocapp.ui.common

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.example.amiddocapp.ui.common.AmidTitle

@Composable
fun AddTextButton(
    modifier: Modifier = Modifier,
    buttonTitle:String = "Добавить",
    onClick:() -> Unit,
    color: Color = MaterialTheme.colorScheme.primary,
    fontWeight: FontWeight = FontWeight.Bold,
    fontSize: TextUnit = 18.sp,
    textAlign: TextAlign = TextAlign.Start,
    overflow: TextOverflow = TextOverflow.Ellipsis,
    singleLine: Boolean = true,
) {
    TextButton(
        onClick = onClick,
        modifier = modifier,
    ) {
        AmidTitle(
            titleString = buttonTitle,
            color = color,
            fontWeight = fontWeight,
            fontSize = fontSize,
            textAlign = textAlign,
            overflow = overflow,
            singleLine = singleLine
        )
    }
}