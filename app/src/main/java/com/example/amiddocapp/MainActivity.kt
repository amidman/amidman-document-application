package com.example.amiddocapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.example.amiddocapp.ui.navigation.MainNavigation
import com.example.amiddocapp.ui.screens.NavGraphs
import com.example.amiddocapp.ui.screens.destinations.LoginScreenDestination
import com.example.amiddocapp.ui.screens.destinations.StudentScreenDestination
import com.example.amiddocapp.ui.screens.login.LoginScreen
import com.example.amiddocapp.ui.screens.student.StudentScreen
import com.example.amiddocapp.ui.theme.AmiddocappTheme
import com.example.amiddocapp.ui.util.collectFlow
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import com.ramcosta.composedestinations.navigation.navigate
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.compose.inject

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AmiddocappTheme {
                // A surface container using the 'background' color from the theme


                val mainNav = rememberNavController()

                val mainNavigation by inject<MainNavigation>()

                mainNavigation.navFlow.onEach {
                    mainNav.navigate(it)
                }.collectFlow()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                    DestinationsNavHost(navGraph = NavGraphs.root, navController = mainNav){
                        composable(LoginScreenDestination){
                            LoginScreen(destinationsNavigator)
                        }

                        composable(StudentScreenDestination){
                            StudentScreen(destinationsNavigator)
                        }

                    }

                }
            }
        }
    }
}

